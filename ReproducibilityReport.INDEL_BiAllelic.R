#!/cm/shared/apps/R/3.0.1/bin/Rscript
# This is running R-3.0.1

args = commandArgs(trailingOnly=TRUE)
print(args)

# running the script
# Rscript --vanilla ReproducibilityReport.R Holland_MendelianDisorders_SeqWholeExome_033117_1
# R CMD BATCH ReproducibilityReport.R Holland_MendelianDisorders_SeqWholeExome_033117_1

proj=args[1]
pairfile=args[2]
# proj="Haiman_ProstateCa_SeqWholeExome_080814_1"
dir <- paste0("/mnt/research/active/",proj)
# dir <- paste0("Y:/Seq_Proj/",proj)
# pairfile= file.path(dir, "Working_HL/StudyDup/Prg/Pairs.csv")


pairs <- read.csv(paste0(pairfile),
                  header=T, stringsAsFactors = F)
(n_pairs <- dim(pairs)[1])
cutoff_DP <- 8


# This is to calculate Concordance for StudyDup for all INDELs without any filter. 
Report_INDEL.all <- data.frame(); Report_INDEL.all.dp <- data.frame()
# for (i in 1:n_pairs){
for (i in 1:n_pairs){
    # Read in vcf files
    (SMtag1 <- pairs[i,1])
    (SMtag2 <- pairs[i,2])
    
    vcf_1_all <- read.table(paste0(dir, "/Working_HL/StudyDup/VCF_Ind/", SMtag1, ".INDEL.vcf"),
                        sep="\t", header=F, stringsAsFactors = F, comment.char = "#")
    vcf_2_all <- read.table(paste0(dir, "/Working_HL/StudyDup/VCF_Ind/", SMtag2, ".INDEL.vcf"),
                        sep="\t", header=F, stringsAsFactors = F, comment.char = "#")
    names(vcf_1_all) <- c("chr", "pos", "rs", "ref", "alt", 
                      "qual", "filter", "info", "format", "sample")
    names(vcf_2_all) <- c("chr", "pos", "rs", "ref", "alt", 
                      "qual", "filter", "info", "format", "sample")
    vcf_1 <- vcf_1_all[!grepl(",", vcf_1_all$alt), ]
    vcf_2 <- vcf_2_all[!grepl(",", vcf_2_all$alt), ]
    
    # get DP position in format
    vcf1_format <- strsplit(vcf_1$format, ":"); vcf2_format <- strsplit(vcf_2$format, ":")
    f2 <- function(x){
      which(x=="DP")
    }
    pos.dp <- sapply(vcf1_format, f2); vcf_1$pos.dp <- pos.dp
    pos.dp <- sapply(vcf2_format, f2); vcf_2$pos.dp <- pos.dp
    
    # get dp for each variant
    vcf1_sample <- strsplit(vcf_1$sample, ":")
        lp <- length(vcf1_sample)
        dp.all <- vector()
        for (i in 1:lp){
          pos <- unlist(vcf_1$pos.dp[i]) 
          dp <- ifelse(length(pos)==0, "0", vcf1_sample[[i]][pos]) 
          dp.all <- c(dp.all, dp)
          }
        vcf_1$dp <- as.numeric(dp.all)
    vcf2_sample <- strsplit(vcf_2$sample, ":")
        lp <- length(vcf2_sample)
        dp.all <- vector()
        for (i in 1:lp){
          pos <- unlist(vcf_2$pos.dp[i])
          dp <- ifelse(length(pos)==0, "0" ,vcf2_sample[[i]][pos]) 
          dp.all <- c(dp.all, dp)
        }
        vcf_2$dp <- as.numeric(dp.all)
  
      
    # get subset of VCF based on informative columns  
    vcf_1$GT <- sapply(strsplit(as.character(vcf_1$sample),':'), "[", 1)
    vcf_2$GT <- sapply(strsplit(as.character(vcf_2$sample),':'), "[", 1)
    sel <- c("chr", "pos","rs", "ref", "alt", "qual", "filter", "GT", "dp")
    subset.vcf_1 <- vcf_1[,sel]; subset.vcf_2 <- vcf_2[,sel]
    subsetDP.vcf_1 <- vcf_1[vcf_1$dp>=cutoff_DP,sel];subsetDP.vcf_2 <- vcf_2[vcf_2$dp>=cutoff_DP,sel]
      
    ## All Sites
    # merge 2 vcf files and compare GTs
    comp <- merge(subset.vcf_1, subset.vcf_2, by=c("chr", "pos", "ref"), all=T)
    GT1 <- comp$GT.x
    GT2 <- comp$GT.y
    compGT <- ifelse(GT1 == GT2, "same", "diff")
    compGT <- ifelse(is.na(GT1), "missing_sample1", compGT)
    compGT <- ifelse(is.na(GT2), "missing_sample2", compGT)
    table(compGT)                
    comp$compGT <- compGT
    results <- as.data.frame(table(compGT))
    
    # Get reproducibility
    diff <- results$Freq[results$compGT=="diff"]
    same <- results$Freq[results$compGT=="same"]
    missing_sample1 <- results$Freq[results$compGT=="missing_sample1"]
    missing_sample2 <- results$Freq[results$compGT=="missing_sample2"]
    total <- same + diff + missing_sample1 + missing_sample2
      
    Reprod_narrow <- same/(same + diff)
    Reprod_wide <- same/total
    missingRate_sample1 <- missing_sample1/total
    missingRate_sample2 <- missing_sample2/total
    
    Report_INDEL <- cbind(SMtag1, SMtag2, diff, same, missing_sample1, missing_sample2,
                        total, Reprod_narrow, Reprod_wide, missingRate_sample1, missingRate_sample2)
    Report_INDEL.all <- rbind(Report_INDEL.all, Report_INDEL)
    
    
    ## Sites with DP greater than cutoff
    # merge 2 vcf files and compare GTs
    comp <- merge(subsetDP.vcf_1, subsetDP.vcf_2, by=c("chr", "pos", "ref"), all=T)
    GT1 <- comp$GT.x
    GT2 <- comp$GT.y
    compGT <- ifelse(GT1 == GT2, "same", "diff")
    compGT <- ifelse(is.na(GT1), "missing_sample1", compGT)
    compGT <- ifelse(is.na(GT2), "missing_sample2", compGT)
    table(compGT)                
    comp$compGT <- compGT
    results <- as.data.frame(table(compGT))
    
    # Get reproducibility
    diff <- results$Freq[results$compGT=="diff"]
    same <- results$Freq[results$compGT=="same"]
    missing_sample1 <- results$Freq[results$compGT=="missing_sample1"]
    missing_sample2 <- results$Freq[results$compGT=="missing_sample2"]
    total <- same + diff + missing_sample1 + missing_sample2
    
    Reprod_narrow <- same/(same + diff)
    Reprod_wide <- same/total
    missingRate_sample1 <- missing_sample1/total
    missingRate_sample2 <- missing_sample2/total
    
    Report_INDEL <- cbind(SMtag1, SMtag2, diff, same, missing_sample1, missing_sample2,
                        total, Reprod_narrow, Reprod_wide, missingRate_sample1, missingRate_sample2)
    Report_INDEL.all.dp <- rbind(Report_INDEL.all.dp, Report_INDEL)
}

write.csv(Report_INDEL.all, paste0(dir, "/Working_HL/StudyDup/Output/Reproducibility_WithinStudy_INDELBiAllelic_PASS.csv"),
          quote=F, row.names=F)
write.csv(Report_INDEL.all.dp, paste0(dir, "/Working_HL/StudyDup/Output/Reproducibility_WithinStudy_INDELBiAllelic_PASS_DP",cutoff_DP,".csv"),
          quote=F, row.names=F)








