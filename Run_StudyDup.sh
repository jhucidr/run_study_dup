#!/bin/bash

MultiSampleVCF=$1
PairFile=$2

module load java/1.8.0_112
module load R

Proj=`echo $1 | cut -d"/" -f5`
echo "Project name is: " $Proj

echo "Pulling out individual sample VCF files"
./GetIndVCF_NonRef.sh $MultiSampleVCF $PairFile

echo "Running Reproducibility report" 
Rscript --vanilla ReproducibilityReport.SNV.R $Proj $PairFile
Rscript --vanilla ReproducibilityReport.INDEL.R $Proj $PairFile
Rscript --vanilla ReproducibilityReport.INDEL_BiAllelic.R $Proj $PairFile


