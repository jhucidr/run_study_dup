Readme -- Create reproducibility report for study duplicate pairs for Holland 
~ Hua Ling ~ 2018.Feb20. email hling1@jhmi.edu for any questions

Notes: Everything runs under linux
########## 
# One-step run
##########
Step1: Create a file with SMtags of study duplicate pair. Each row is a pair of samples. 
	* eg Y:\Seq_Proj\Holland_MendelianDisorders_SeqWholeExome_033117_1\Working_HL\StudyDup\Pairs_file.csv
	
Go to /isilon/sequencing/Seq_Proj/XXX/Working_HL/Study_Dup/Prg
	* Type in "./Run_StudyDup.sh PathToMultiSampleVCF PathToDupPairFile" 
		eg: ./Run_StudyDup.sh /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/MULTI_SAMPLE/Holland_2018_02_15_70samples.BEDsuperset.VQSR.1KG.ExAC3.REFINED.vcf /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/Working_HL/StudyDup/Pairs_file.csv













########## 
# Multi-step run
##########
Step1: Create a file with SMtags of study duplicate pair. Each row is a pair of samples. 
	* eg Y:\Seq_Proj\Holland_MendelianDisorders_SeqWholeExome_033117_1\Working_HL\StudyDup\Pairs_file.csv
	
Step2: Pull out individual sample VCF files from multi-sample VCF file. 
	* Go to Prg directory: eg: Y:\Seq_Proj\Holland_MendelianDisorders_SeqWholeExome_033117_1\Working_HL\StudyDup\Prg
			eg: cd /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/Working_HL/StudyDup/Prg
	* Type in "module load java/1.8.0_112"
	* ./GetIndVCF_NonRef FullPath_To_Multi-sampleVCF PathToPairsFile
			eg: ./GetIndVCF_NonRef.sh /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/MULTI_SAMPLE/Holland_2018_02_15_70samples.BEDsuperset.VQSR.1KG.ExAC3.REFINED.vcf ../Pairs_file.csv
	* You will see 2 times of duplicate pairs VCF files created under /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/Working_HL/StudyDup/VCF_Ind
			
Step3: Create Reproducibility Report
	* module load R
	* Rscript --vanilla ReproducibilityReport.SNV.R "name of the project" "PathToPairFile"
			eg: Rscript --vanilla ReproducibilityReport.SNV.R Holland_MendelianDisorders_SeqWholeExome_033117_1 /isilon/sequencing/Seq_Proj/Holland_MendelianDisorders_SeqWholeExome_033117_1/Working_HL/StudyDup/Pairs_file.csv
	* You should see 2 reports named as "Reproducibility_WithinStudy_XXX_PASS.csv" and "Reproducibility_WithinStudy_XXX_PASS_DP8.csv" 
			* Reproducibility_WithinStudy_XXX_PASS.csv: is based on all PASS variants
			* Reproducibility_WithinStudy_XXX_PASS_DP8.csv: is based on all PASS and DP>=8. 
			/