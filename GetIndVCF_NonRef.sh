#!/bin/bash

VCF_Input=$1
List_SMtag=$2

Proj=`echo $VCF_Input | cut -d"/" -f5`
echo $Proj
echo "DupPair file: " $2

dos2unix $2

sed '1d' $2 | awk -F"," '{print $1}' > temp1.txt
sed '1d' $2 | awk -F"," '{print $2}' > temp2.txt
cat temp2.txt temp1.txt > IDlist.txt

for SMtag in `cat IDlist.txt`;
do

java -Xmx20g -jar /cm/shared/apps/gatk/3.7/GenomeAnalysisTK.jar \
	-R /mnt/research/tools/PIPELINE_FILES/bwa_mem_0.7.5a_ref/human_g1k_v37_decoy.fasta \
  -T SelectVariants \
  -selectType SNP \
  --variant $VCF_Input \
  -o /mnt/research/active/$Proj/Working_HL/StudyDup/VCF_Ind/${SMtag}.SNV.vcf \
  --sample_name $SMtag \
  -ef -env

java -Xmx20g -jar /cm/shared/apps/gatk/3.7/GenomeAnalysisTK.jar \
	-R /mnt/research/tools/PIPELINE_FILES/bwa_mem_0.7.5a_ref/human_g1k_v37_decoy.fasta \
  -T SelectVariants \
  --selectTypeToExclude SNP \
  --variant $VCF_Input \
  -o /mnt/research/active/$Proj/Working_HL/StudyDup/VCF_Ind/${SMtag}.INDEL.vcf \
  --sample_name $SMtag \
  -ef -env
  
done
